//https://knasmueller.net/vue-js-on-gitlab-pages
module.exports = {
  publicPath: process.env.NODE_ENV === "production" ? "/wildimg/" : "/"
};
